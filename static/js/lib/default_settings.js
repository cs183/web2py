// This is the js for the default/index.html view.
//base from lecture
var app = function () {

    var self = {};

    Vue.config.silent = false; // show all warnings

    // Extends an array
    self.extend = function (a, b) {
        for (var i = 0; i < b.length; i++) {
            a.push(b[i]);
        }
    };

    var enumerate = function (v) {
        var k = 0;
        return v.map(function (e) {
            e._idx = k++;
        });
    };

    self.open_uploader = function () {
        $("div#uploader_div").show();
        self.vue.is_uploading = true;
    };

    self.close_uploader = function () {
        $("div#uploader_div").hide();
        self.vue.is_uploading = false;
        $("input#file_input").val(""); // This clears the file choice once uploaded.

    };

    self.upload_file = function (event) {
        // Reads the file.
        var input = $("input#file_input")[0];
        var file = input.files[0];
        if (file) {
            // First, gets an upload URL.
            console.log("Trying to get the upload url");
            $.getJSON('https://upload-dot-luca-teaching.appspot.com/start/uploader/get_upload_url',
                function (data) {
                    // We now have upload (and download) URLs.
                    var put_url = data['signed_url'];
                    var get_url = data['access_url'];
                    console.log("Received upload url: " + put_url);
                    // Uploads the file, using the low-level interface.
                    var req = new XMLHttpRequest();
                    req.addEventListener("load", self.upload_complete(get_url));
                    req.open("PUT", put_url, true);
                    req.send(file);
                });
        }
    };

    self.upload_complete = function (get_url) {
        // Hides the uploader div.
        self.close_uploader();
        console.log('The file was uploaded; it is now available at ' + get_url);
        // TODO: The file is uploaded.  Now you have to insert the get_url into the database, etc.
        $.post(add_track_url,
            {
                track_url: get_url,
                artist: self.vue.form_artist,
                album: self.vue.form_album,
                title: self.vue.form_title,
            },
            function (data) {
                setTimeout(function () {
                    self.vue.tracks.unshift(data.track);
                    enumerate(self.vue.tracks);
                    self.vue.form_artist = "";
                    self.vue.form_album = "";
                    self.vue.form_title = "";
                }, 1000);
            });
    };


    self.get_tracks = function (user) {
        self.vue.current_user = user;
        $.getJSON(get_tracks_url,
            {
                user: user,
            },
            function (data) {
            self.vue.tracks = data.tracks;
            enumerate(self.vue.tracks);
        });
    };

    self.get_user = function () {
        $.getJSON(get_user_url,
            function (data) {
                self.vue.users = data.users;
                self.vue.first_name = data.first_name;
                self.vue.last_name = data.last_name;
                enumerate(self.vue.users);
            });
    }


    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
            tracks: [],
            users: [],
            first_name: "",
            last_name: "",
            current_user: "",
        },
        methods: {
            get_tracks: self.get_tracks,
            get_user: self.get_user,
        }

    });

    self.get_tracks('default');
    self.get_user();
    $("#vue-div").show();

    return self;
};

var APP = null;

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
jQuery(function () {
    APP = app();
});
