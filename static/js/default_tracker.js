// This is the js for the default/tracker.html view.

var app = function() {

    var self = {};

    Vue.config.silent = false; // show all warnings

    var enumerate = function(v) { var k=0; return v.map(function(e) {e._idx = k++;});};
    // Extends an array
    self.extend = function(a, b) {
        for (var i = 0; i < b.length; i++) {
            a.push(b[i]);
        }
    };

    self.select_date = function(date){
        self.vue.date = date;
        $.getJSON(get_date_url,
            {date: date},
            function(data){
                self.vue.day_data = data.day_data;
                console.log(self.vue.day_data);
                self.vue.date = data.date;
                self.vue.form_attacks = "";
                self.is_editing_date = false;
                self.vue.form_notes = "";
            }
        );
    };

    self.add_date = function(chart, calendar){
        console.log(chart);
        $.post(add_date_url,
            {
                date: self.vue.date,
                attacks: self.vue.form_attacks,
                notes: self.vue.form_notes
            },
            function(data){
                self.vue.day_data = data.day_data;
                self.vue.form_attacks = "";
                self.vue.form_notes = "";
                $.getJSON(get_dates_url,
                  function(data){
                    chart.data.datasets[0].data = data.data.map(function(t){
                        var date = t.date.split("/");
                        var newdate = date[2]+'-';

                        if(date[0].length == 1){
                            newdate+='0'+date[0]+'-';
                        }else{
                            newdate+=date[0]+'-';
                        }

                        if(date[1].length == 1){
                            newdate+='0'+date[1];
                        }else{
                            newdate+=date[1];
                        }
                        dates.push(new Date(newdate));
                        return({
                            t: new Date(newdate),
                            y: t.attacks
                        });
                    });
                    done(chart);
                    calendar.attrs = data.data.map(function(t){
                    var date = t.date.split("/");
                    return({
                            key: t.id,
                            highlight: {
                            backgroundColor: '#87CBF7',
                            borderColor: 'white',
                            borderWidth: '2px',
                            borderStyle: 'solid'
                        },
                        contentStyle:{
                            color: 'white',
                        },
                        dates: new Date(date[2], date[0]-1, date[1]),
                    })})
                  }
                );
            });
    }

    var done = function(chart){
        chart.update();
    }

    self.edit_button = function(){
        if(self.vue.is_editing_date){
            self.vue.day_data.attacks = self.vue.old_attacks;
            self.vue.day_data.notes = self.vue.old_notes;
        }else{
            self.vue.old_attacks = self.vue.day_data.attacks;
            self.vue.old_notes = self.vue.day_data.notes;
        }
        self.vue.form_attacks = "";
        self.vue.form_notes = "";
        self.vue.is_editing_date = !self.vue.is_editing_date;
    }

    self.edit_date = function(chart){
        console.log(self.vue.date);
        $.post(edit_date_url,
            {
                date: self.vue.date,
                attacks: self.vue.form_attacks,
                notes: self.vue.form_notes
            },
            function(data){
                self.vue.day_data = data.day_data;
                $.getJSON(get_dates_url,
                  function(data){
                    chart.data.datasets[0].data = data.data.map(function(t){
                        var date = t.date.split("/");
                        var newdate = date[2]+'-';

                        if(date[0].length == 1){
                            newdate+='0'+date[0]+'-';
                        }else{
                            newdate+=date[0]+'-';
                        }

                        if(date[1].length == 1){
                            newdate+='0'+date[1];
                        }else{
                            newdate+=date[1];
                        }
                        dates.push(new Date(newdate));
                        return({
                            t: new Date(newdate),
                            y: t.attacks
                        });
                    });
                    done(chart);
                  }
                );
                self.vue.form_attacks = "";
                self.vue.form_notes = "";
                self.vue.is_editing_date = false;
            });
    }

    // Complete as needed.
    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
            day_data: null,
            date: 'default',
            form_attacks: '',
            form_notes: '',
            is_editing_date: false,
            old_attacks: '',
            old_notes: '',
        },
        methods: {
            select_date: self.select_date,
            add_date: self.add_date,
            add_button: self.add_button,
            edit_button: self.edit_button,
            edit_date: self.edit_date,
        }

    });

    self.select_date('default');
    $("#vue-div").show();
    return self;
};

var APP = null;

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
jQuery(function(){APP = app();});
