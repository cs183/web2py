// This is the js for the default/index.html view.
//base from lecture
var app = function () {

    var self = {};

    Vue.config.silent = false; // show all warnings

    // Extends an array
    self.extend = function (a, b) {
        for (var i = 0; i < b.length; i++) {
            a.push(b[i]);
        }
    };

    var enumerate = function (v) {
        var k = 0;
        return v.map(function (e) {
            e._idx = k++;
        });
    };

    self.insertion_id = null;

    self.add_track_button = function () {
        self.vue.is_adding_track_info = !self.vue.is_adding_track_info;
        self.vue.is_adding_track = !self.vue.is_adding_track;
        if(self.vue.is_adding_track && self.vue.is_adding_track_info){
            $("div#uploader_div").show();
        }else{
            $("div#uploader_div").hide();
            self.vue.form_album = "";
            self.vue.form_artist = "";
            self.vue.form_title = "";
            self.vue.form_genre = "";
        }
    };

    self.add_track = function () {
        // Submits the track info.
        // This is the last step of the track insertion process.
        $.post(add_track_url,
            {
                artist: self.vue.form_artist,
                title: self.vue.form_title,
                album: self.vue.form_album,
                genre: self.vue.form_genre,
                insertion_id: self.insertion_id
            },
            function (data) {
                $.web2py.enableElement($("#add_track_submit"));
                self.vue.tracks.unshift(data.track);
                enumerate(self.vue.tracks);
                self.vue.form_artist = "";
                self.vue.form_title = "";
                self.vue.form_album = "";
                self.vue.is_adding_track_info = false;
                self.vue.form_genre = "";
            });
    };

    self.upload_file = function (event) {
        // Reads the file.
        var input = $("input#file_input")[0];
        var file = input.files[0];
        if (file) {
            $.getJSON(upload_url,
                {file: file},
                function (data) {

                });
        }
    };

    self.upload_complete = function (response) {
        self.insertion_id = response.insertion_id;
        self.vue.is_adding_track_info = true;
        self.vue.is_adding_track = false;
        $("div#uploader_div").hide();
    };


    self.delete_track = function (track_idx) {
        $.post(del_track_url,
            {track_id: self.vue.tracks[track_idx].id},
            function () {
                self.vue.tracks.splice(track_idx, 1);
                enumerate(self.vue.tracks);
            }
        )
    };


    self.select_track = function (track_idx) {
        var track = self.vue.tracks[track_idx];
        if (self.vue.selected_id === track.id) {
            // Deselect track.
            self.vue.selected_id = -1;
        } else {
            // Select it.
            self.vue.selected_idx = track_idx;
            self.vue.selected_id = track.id;
            self.vue.selected_url = track.track_url;
        }
        if (self.vue.selected_url && self.vue.selected_id > -1) {
            $.post(play_url,
                {track_id: track.id},
                function () {}
            );
        }
    };


    self.toggle_favorite = function (track_idx, favorite_idx) {
        var track = self.vue.tracks[track_idx];
        $.post(toggle_favorite_url,
            {track_id: track.id},
            function (data) {
                self.vue.favorites = data.favorites;
                enumerate(self.vue.favorites);
            }
        )
    };

    self.toggle_public = function(track_idx){
        var track = self.vue.tracks[track_idx];
        track.is_public = !track.is_public;
        $.post(toggle_url,
            {track_id: track.id},
            function () {
            }
        )
    }

    self.favorited = function(track_idx){
        var track = self.vue.tracks[track_idx];
        for(var i = 0; i<self.vue.favorites.length; i++){
            if(track.id == self.vue.favorites[i].track_id){
                return true;
            }
        }
        return false;
    }

    self.get_tracks = function (user) {
        $.getJSON(tracks_url,
            {
                user: user,
            },
            function (data) {
                self.vue.tracks = data.tracks;
                self.vue.current_user = data.user;
                self.vue.favorites = data.favorites;
                enumerate(self.vue.favorites);
                enumerate(self.vue.tracks);
            })
    };

    self.search_tracks = function(search){
        console.log(search);
        $.getJSON(search_url,
            {
                search: search,
                user: self.vue.current_user
            },
            function (data) {
                self.vue.tracks = data.tracks;
                enumerate(self.vue.tracks);
                self.vue.form_search = "";
            })
    }

    self.get_user = function () {
        $.getJSON(get_user_url,
            function (data) {
                self.vue.users = data.users;
                self.vue.first_name = data.first_name;
                self.vue.last_name = data.last_name;
                enumerate(self.vue.users);
            });
    }

    self.get_favorites = function(){
        $.getJSON(get_favorites_url,
                  {user: self.vue.current_user},
                  function(data){
                    self.vue.tracks = data.tracks;
                    enumerate(self.vue.tracks);
                  })
    }


    self.vue = new Vue({
        el: "#vue-div",
        delimiters: ['${', '}'],
        unsafeDelimiters: ['!{', '}'],
        data: {
            tracks: [],
            favorites: [],
            users: [],
            first_name: "",
            last_name: "",
            current_user: "",
            is_uploading: false,
            form_artist: "",
            form_album: "",
            form_title: "",
            selected_id: -1,
            is_adding_track: false,
            is_adding_track_info: false,
            is_being_played: false,
            form_genre: null,
            selected_url: null,
            form_search: null,
        },
        methods: {
            get_tracks: self.get_tracks,
            get_user: self.get_user,
            add_track_button: self.add_track_button,
            add_track: self.add_track,
            upload_file: self.upload_file,
            upload_complete: self.upload_complete,
            delete_track: self.delete_track,
            select_track: self.select_track,
            toggle_favorite: self.toggle_favorite,
            search_tracks: self.search_tracks,
            get_favorites: self.get_favorites,
            toggle_public: self.toggle_public,
            favorited: self.favorited
        }

    });

    self.get_tracks('default');
    self.get_user();
    $("#vue-div").show();

    return self;
};

var APP = null;

// This will make everything accessible from the js console;
// for instance, self.x above would be accessible as APP.x
jQuery(function () {
    APP = app();
});
