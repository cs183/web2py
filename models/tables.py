# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.

import datetime

def get_user_email():
    return auth.user.email if auth.user is not None else None

def get_user_id():
    return auth.user.id if auth.user is not None else None

#if there's anything else you think would be helpful to put in here, feel free to add to it
db.define_table('day_data',
                Field('hizuke', 'string'), #DATE/TIME/DAY IS A RESERVED WORD I GAVE UP ON FINDING OTHER SYNONYMS OK AHHH
                Field('attacks', 'integer'),
                Field('notes', 'text'),
                Field('user_email', default = get_user_email()),
                )

#Base from lecture
db.define_table('track',
                Field('artist'),
                Field('album'),
                Field('title'),
                Field('genre'),
                Field('created_by', default=get_user_id()),
                Field('created_on', default=datetime.datetime.utcnow()),
                Field('is_public', 'boolean', default=False)
                )

db.define_table('track_data',
                Field('track_id', 'reference track'),
                Field('original_filename'),
                Field('data_blob', 'blob'),
                Field('mime_type'),
                Field('insertion_id')
                )
db.define_table('favorites',
                Field('track_id'),
                Field('favorited_by', default=get_user_id))

# after defining tables, uncomment below to enable auditing
auth.enable_record_versioning(db)
