import tempfile
import datetime

# Cloud-safe of uuid, so that many cloned servers do not all use the same uuids.
from gluon.utils import web2py_uuid


# Here go your api methods.


@auth.requires_login()
def get_date():
    if auth.user is not None:
        date = request.vars.date
        day_data = None
        if (date == 'default'):
            time = datetime.datetime.today()
            month = str(time.month)
            day = str(time.day)
            year = str(time.year)
            date = month + "/" + day + "/" + year
            day_data = db((db.day_data.hizuke == date) &
                          (db.day_data.user_email == auth.user.email)).select().first()
        else:
            day_data = db((db.day_data.hizuke == date) &
                          (db.day_data.user_email == auth.user.email)).select().first()
        return response.json(dict(
            day_data=day_data,
            date=date
        ))


@auth.requires_login()
def get_dates():
    dates = db(db.day_data.user_email == auth.user.email).select()
    data = []
    for i in dates:
        d = dict(
            id = i.id,
            date = i.hizuke,
            attacks = i.attacks,
            )
        data.append(d)
    data.sort(key = lambda x: datetime.datetime.strptime(x['date'], '%m/%d/%Y'))
    return response.json(dict(
        data = data))

@auth.requires_login()
def add_date():
    attacks = request.vars.attacks
    date = request.vars.date
    notes = request.vars.notes
    d_id = db.day_data.insert(
        attacks=attacks,
        hizuke=date,
        notes=notes
    )
    d = db.day_data(d_id)
    return response.json(dict(day_data=d))


@auth.requires_login()
def edit_date():
    if auth.user is not None:
        attacks = request.vars.attacks
        date = request.vars.date
        notes = request.vars.notes
        d = db((db.day_data.hizuke == date) &
                (db.day_data.user_email == auth.user.email)).select().first()
        d.update_record(
            attacks=attacks,
            notes=notes,
            hizuke=date
        )
        return response.json(dict(day_data=d))



def build_track_url(i):
    return URL('api', 'play_track', vars=dict(track_id=i), user_signature=True)


# base from lecture
@auth.requires_signature(hash_vars=False)
def get_tracks():
    if auth.user is not None:
        tracks = []
        user = request.vars.user
        rows = []
        if user == 'default':
            rows = db((db.track.created_by == auth.user.id) |
                    (db.track.is_public == True)).select(db.track.ALL)
        else:
            rows = db((db.track.created_by == user)).select()
        for i, r in enumerate(rows):
            t = dict(
                id=r.id,
                track_url=build_track_url(r.id),
                artist=r.artist,
                album=r.album,
                title=r.title,
                genre=r.genre,
                is_public = r.is_public,
                created_by = r.created_by,
            )
            tracks.append(t)
        user = auth.user.id
        rows = db(db.favorites.favorited_by == user).select()
        favorites = []
        for i, r in enumerate(rows):
            t = dict(
                id=r.id,
                track_id = r.track_id
            )
            favorites.append(t)
        return response.json(dict(
            favorites = favorites,
            user = user,
            tracks = tracks
        ))

@auth.requires_login()
def search():
    if auth.user is not None:
        tracks = []
        search = request.vars.search
        user = request.vars.user
        rows = db(((db.track.artist == search) | (db.track.title == search) |
                   (db.track.album == search) | (db.track.genre == search)) &
                   ((db.track.is_public == True) |
                    (db.track.created_by == user))).select(db.track.ALL)
        for i, r in enumerate(rows):
            t = dict(
                id=r.id,
                track_url=build_track_url(r.id),
                artist=r.artist,
                album=r.album,
                title=r.title,
                genre=r.genre,
                created_by = r.created_by,
                is_public = r.is_public
            )
            tracks.append(t)
        return response.json(dict(
            tracks=tracks
        ))


@auth.requires_login()
def get_favorites():
    if auth.user is not None:
        tracks = []
        user = request.vars.user
        rows = db((db.favorites.track_id == db.track.id) & (db.favorites.favorited_by == user) &
                  ((db.track.is_public == True) |
                  (db.track.created_by == user))).select(db.track.ALL)
        for i, r in enumerate(rows):
            t = dict(
                id=r.id,
                track_url=build_track_url(r.id),
                artist=r.artist,
                album=r.album,
                title=r.title,
                genre=r.genre,
                created_by = r.created_by,
                is_public = r.is_public
            )
            tracks.append(t)
        return response.json(dict(
            tracks=tracks
        ))

@auth.requires_signature()
def get_insertion_id():
    insertion_id = web2py_uuid()
    return response.json(dict(
        insertion_id=insertion_id
    ))


@auth.requires_signature()
def upload_track():
    logger.info("_signature: %r", request.vars._signature)
    logger.info("Track insertion id: %r", request.vars.insertion_id)
    db(db.track_data.track_id == None).delete()
    logger.info("Uploaded a file of type %r" % request.vars.file.type)
    if not request.vars.file.type.startswith('audio'):
        raise HTTP(500)
    insertion_id = db.track_data.insert(
        track_id=None,
        original_filename=request.vars.file.filename,
        data_blob=request.vars.file.file.read(),
        mime_type=request.vars.file.type,
        insertion_id=request.vars.insertion_id,
    )
    return response.json(dict(
        insertion_id=insertion_id
    ))

# base from lecture
@auth.requires_login()
def add_track():
    t_id = db.track.insert(
        artist=request.vars.artist,
        album=request.vars.album,
        title=request.vars.title,
        genre=request.vars.genre,
    )
    db(db.track_data.id == request.vars.insertion_id).update(track_id=t_id)
    db(db.track_data.track_id == None).delete()
    return response.json(dict(track=dict(
        id=t_id,
        artist=request.vars.artist,
        album=request.vars.album,
        title=request.vars.title,
        is_public = False,
        created_by = auth.user.id,
        genre=request.vars.genre,
        track_url=build_track_url(t_id)
    )))


# base from lecture
@auth.requires_login()
def del_track():
    db(db.track.id == request.vars.track_id).delete()
    return "ok"


@auth.requires_signature()
def toggle_favorite():
    track_id = request.vars.track_id
    t = db((db.favorites.track_id == track_id) &
           (db.favorites.favorited_by == auth.user.id)).select().first()
    if t == None:
        db.favorites.insert(track_id=track_id)
    else:
        db(db.favorites.id == t.id).delete()
    rows = db(db.favorites.favorited_by == auth.user.id).select()
    favorites = []
    for i, r in enumerate(rows):
            t = dict(
                id=r.id,
                track_id = r.track_id
            )
            favorites.append(t)
    return response.json(dict(favorites = favorites))

@auth.requires_signature()
def toggle_public():
    t = db.track(request.vars.track_id)
    t.update_record(is_public=not t.is_public)
    return "ok"


@auth.requires_signature()
def cleanup():
    db(db.track_data.track_id == None).delete()


@auth.requires_signature()
def delete_music():
    track_id = request.vars.track_id
    if track_id is None:
        raise HTTP(500)
    db(db.track_data.track_id == track_id).delete()
    return "ok"

@auth.requires_signature()
def play_track():
    track_id = int(request.vars.track_id)
    t = db(db.track_data.track_id == track_id).select().first()
    if t is None:
        return HTTP(404)
    headers = {}
    headers['Content-Type'] = t.mime_type
    f = tempfile.NamedTemporaryFile()
    f.write(t.data_blob)
    f.seek(0)
    return response.stream(f.name, chunk_size=4096, request=request)


def get_user():
    if auth.user is not None:
        users = []
        rows = db((db.auth_user.id != auth.user.id)).select(db.auth_user.ALL)
        for i, r in enumerate(rows):
            u = dict(
                first_name=r.first_name,
                last_name=r.last_name,
                id=r.id,
            )
            users.append(u)
        first_name = auth.user.first_name
        last_name = auth.user.last_name
        return response.json(dict(
            users=users,
            first_name=first_name,
            last_name=last_name
        ))



